<?php

namespace Gini\ORM;

class Article extends Object
{
    public $title = 'string:120';
    public $content = 'blob:long';
    public $pub_date = 'string:120';
    public $pub_person = 'string:120';

    public static $db_index = [
        'unique:title',
        'pub_date',
    ];

    public function save()
    {
        return parent::save();
    }
}

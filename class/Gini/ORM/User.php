<?php

namespace Gini\ORM;

class User extends Object
{
    public $name = 'string:120';
    public $username = 'string:120';
    public $email = 'string:120';
    public $password = 'string:120';

    public static $db_index = [
        'name',
        'unique:username',
        'unique:email',
    ];

    public function save()
    {
        return parent::save();
    }
}

<?php

namespace Gini\Controller\CGI;

class User extends Layout\Backend
{
    public function __index()
    {
        $this->view->main = V('user/add');
    }
    public function actionForwardAdd()
    {
        $this->view->main = V('user/add');
        $this->view->title = '添加用户';
        $this->view->aside->active = 'user/add';
    }
    public function actionForwardList()
    {
        $this->view->title = '用户列表';
        $users = those('user')->whose('username')->isnot('genee');
        $this->view->main = V('user/list', ['users' => $users]);
        $this->view->aside->active = 'user/list';
    }
    public function actionAdd()
    {
        $form = $this->form('post');
        $validator = new \Gini\CGI\Validator();
        $validator->validate('name', !!$form['name'], T('姓名不能为空!'));
        $validator->validate('username', !!$form['username'], T('用户名不能为空!'));
        $validator->validate('email', !!$form['email'], T('邮箱不能为空!'));
        if (!preg_match('/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/', $form['email'])) {
            $validator->validate('email', false, T('邮箱格式不正确!'));
        }
        if (a('user')->whose('email')->is($form['email'])->id != '') {
            $validator->validate('email', false, T('该邮箱已注册!'));
        }
        try {
            $validator->done();
            $user = a('user');
            $user->name = $form['name'];
            $user->email = $form['email'];
            $user->username = $form['username'];
            $password = \Gini\Module\Help::create_password();
            $user->password = md5($password);
            $user->save();
            \Gini\Module\Help::sendemail($user, $password);
            $this->redirect('/user/forwardlist');
        } catch (\Gini\CGI\Validator\Exception $e) {
            $form['_errors'] = $validator->errors();
        }
        $this->view->title = '添加用户';
        $this->view->aside->active = 'user/add';
        $this->view->main = V('user/add', ['form' => $form]);
    }

    public function actiondelete($id)
    {
        $user = a('user', $id);
        $user->delete();
        $this->redirect('/user/forwardlist');
    }
    public function actionForwardEdit($id)
    {
        $user = a('user', $id);
        $this->view->title = '编辑用户信息';
        $this->view->main = V('user/edit', ['user' => $user]);
    }
    public function actionForwardPassword()
    {
        $user = a('user')->whose('username')->is($_SESSION['username']);
        $this->view->title = '修改密码';
        $this->view->aside->active = 'user/forwardpassword';
        $this->view->main = V('user/mod_password', ['user' => $user]);
    }
    public function actionUpdatePassword($id)
    {
        $user = a('user', $id);
        $form = $this->form('post');
        $validator=new \Gini\CGI\Validator();
        $validator->validate('new',!!$form['new'],T('密码不能为空'));
        $validator->validate('repeat',$form['new']==$form['repeat'],T('密码不一致'));
        try{
            $validator->done();
            $new = $form['new'];
            $repeat = $form['repeat'];
            $user->password = md5($new);
            $user->save();
            $this->redirect('/login/logout');
        }catch(\Gini\CGI\Validator\Exception $e){
            $form['_errors']=$validator->errors();
        }
        $this->view->title = '修改密码';
        $this->view->aside->active = 'user/forwardpassword';
        $this->view->main = V('user/mod_password', ['user' => $user,'form'=>$form]);
    }
    public function actionUpdate($id)
    {
        $user = a('user', $id);
        $form = $this->form('post');
        $user->username = $form['username'];
        $user->name = $form['name'];
        $user->email = $form['email'];
        $user->save();
        $this->redirect('/user/forwardlist');
    }
}

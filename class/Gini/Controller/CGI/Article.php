<?php

namespace Gini\Controller\CGI;

class Article extends Layout\Backend
{
    public function __index()
    {
        $this->view->title = '公告管理';
        $this->view->main = V('article/add');
    }
    public function actionForwardAdd()
    {
        $this->view->title = '添加公告';
        $date = date('Y-m-d');
        $this->view->main = V('article/add', ['date' => $date]);
        $this->view->aside->active = 'article/add';
    }
    public function actionForwardList()
    {
        $articles = those('article')->orderBy('pub_date', 'd');
        if (isset($this->form('get')['pre'])) {
            $cur = $this->form('get')['pre'];
        }
        if (isset($this->form('get')['next'])) {
            $cur = $this->form('get')['next'];
        }

        $cur = ($cur != null) ? $cur : 0;
        $per = 10;
        $pre = ($cur - 1) < 0 ? 0 : ($cur - 1);
        $page_count = floor(count($articles) / $per);
        $next = ($cur + 1) < $page_count ? ($cur + 1) : $page_count;
        $articles = $articles->limit($cur * $per, $per);
        $this->view->title = '公告列表';
        $this->view->main = V('article/list', ['articles' => $articles, 'page_count' => $page_count, 'pre' => $pre, 'next' => $next]);
        $this->view->aside->active = 'article/list';
    }
    public function actionAdd()
    {
        $form = $this->form('form');

        $validator = new \Gini\CGI\Validator();
        $validator->validate('title', !!$form['title'], T('标题不能为空!'));
        $validator->validate('content', !!$form['content'], T('内容不能为空!'));

        try {
            $validator->done();
            $article = a('article');
            $article->title = $form['title'];
            $article->content = $form['content'];
            $article->pub_date = $form['pub_date'];
            $article->pub_person = $form['pub_person'];
            $article->save();
            $this->redirect('/article/forwardlist');
        } catch (\Gini\CGI\Validator\Exception $e) {
            $form['_errors'] = $validator->errors();
        }
        $this->view->title = '添加公告';
        $this->view->aside->active = 'article/add';
        $this->view->main = V('article/add', ['form' => $form, 'date' => date('Y-m-d')]);
    }
    public function actionDelete($id)
    {
        $article = a('article', $id);
        $article->delete();
        $this->redirect('/article/forwardlist');
    }
    public function actionForwardEdit($id)
    {
        $article = a('article', $id);
        $this->view->title = '编辑公告信息';
        $this->view->main = V('article/edit', ['article' => $article]);
    }
    public function actionUpdate($id)
    {
        $form = $this->form('post');
        $article = a('article', $id);
        $validator = new \Gini\CGI\Validator();
        $validator->validate('title', !!$form['title'], T('标题不能为空!'));
        $validator->validate('content', !!$form['content'], T('内容不能为空!'));
        try{
            $validator->done();
            $article->title = $form['title'];
            $article->pub_date = $form['pub_date'];
            $article->content = $form['content'];
            $article->save();
            $this->redirect('/article/forwardlist');
        } catch (\Gini\CGI\Validator\Exception $e) {
            $form['_errors'] = $validator->errors();
        }
        $this->view->title = '编辑公告信息';
        $this->view->main = V('article/edit', ['article'=>$article,'form' => $form, 'date' => date('Y-m-d')]);
    }
}

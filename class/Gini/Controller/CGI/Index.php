<?php

namespace Gini\Controller\CGI {

class Index extends Layout\Base
{
    public function __index()
    {
        $notices_path = \Gini\Core::locateFile('data/announcements/index.json', 'nankai-less');
        $notices = json_decode(file_get_contents($notices_path), true);
        usort($notices, function ($a, $b) {
                if ($a == $b) {
                    return 0;
                }
                if ($a['istop'] > $b['istop']) {
                    return -1;
                } elseif ($a['istop'] < $b['istop']) {
                    return 1;
                } elseif ($a['istop'] == $b['istop']) {
                    $a['date'] = strtotime($a['date']);
                    $b['date'] = strtotime($b['date']);

                    return ($a['date'] > $b['date']) ? -1 : 1;
                }});
        $notices = array_slice($notices, 0, 4);
        $login_url = \Gini\Config::get('login.login');
        $this->view->navbar->active = 'index';
        $this->view->body = V('index', array('notices' => $notices, 'login_url' => $login_url));
        $this->view->title = '南开大学大型仪器管理平台';
    }
}

}

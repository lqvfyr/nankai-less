<?php

namespace Gini\Controller\CGI;

class Login extends Layout\Another
{
    public function __index()
    {
        $this->view->main = V('login');
        $this->view->title = '后台登录';
    }
    public function actionLogin()
    {
        //session_start();
        $form = $this->form('post');
        $validator=new \Gini\CGI\Validator();
        $validator->validate('username',!!$form['username'],T('用户名不能为空'));
        $validator->validate('password',!!$form['password'],T('密码不能为空'));
        try{
            $validator->done();
            $user=a('user')->whose('username')->is($form['username']);
            if ($user->username == $form['username'] && $user->password == md5($form['password'])) {
                session_start();
                $_SESSION['username'] = $form['username'];
                $this->redirect('/admin');
            } else {
                $validator=new \Gini\CGI\Validator();
                $validator->validate('all',false,T('账号和密码不匹配'));
                try{
                    $validator->done();
                }catch(\Gini\CGI\Validator\Exception $e){
                    $form['_errors']=$validator->errors();
                }
            }
        }catch(\Gini\CGI\Validator\Exception $e){
            $form['_errors']=$validator->errors();
        }
        $this->view->main=V('login',['form'=>$form]);
        $this->view->title='后台登录';

    }
    public function actionLogout()
    {
        unset($_SESSION['username']);
        $this->redirect('/login');
    }
    public function actionForgotPwd()
    {
        $this->view->title = '找回密码';
        $this->view->main = V('forgot');
    }
    public function actionFund(){
        $this->view->title='消息';
        $this->view->main=V('message',['msg'=>'请查阅邮件<a href="/admin">返回</a>']);
    }
    public function actionFindPwd()
    {
        $form=$this->form('post');
        $validator=new \Gini\CGI\Validator();
        $validator->validate('email',!!$form['email'],T('邮箱不能为空'));
        if (!preg_match('/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/', $form['email'])) {
             $validator->validate('email', false, T('邮箱格式不正确!'));
        }
        $user = a('user')->whose('email')->is($form['email']);
        if (!$user->id){
             $validator->validate('email', false, T('该邮箱格没有注册!'));
        }
        try{
            $validator->done();
            if($user->id){
                $password = \Gini\Module\Help::create_password();
                $user->password = md5($password);
                $user->save();
                \Gini\Module\Help::sendemail($user, $password);
                $this->redirect('/login/fund');
            }
        }catch(\Gini\CGI\Validator\Exception $e){
            $form['_errors']=$validator->errors();
        }
        $this->view->title = '找回密码';
        $this->view->main = V('forgot',['form'=>$form]);
    }
}

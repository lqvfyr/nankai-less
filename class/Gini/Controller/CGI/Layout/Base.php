<?php

namespace Gini\Controller\CGI\Layout {

    class Base extends \Gini\Controller\CGI\Layout
    {
        public function __preAction($action, &$params)
        {
            parent::__preAction($action, $params);
            $this->view->header = V('header');
            $this->view->navbar = V('navbar');
        }
    }

}

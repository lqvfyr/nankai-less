<?php

namespace Gini\Controller\CGI\Layout;

abstract class Another extends \Gini\Controller\CGI\Layout
{
    public function __preAction($action, &$params)
    {
        $this->view = V('admin/another');
        $this->view->header = V('admin/header', ['site_name_zh' => \Gini\Config::get('site.name_zh'), 'site_name_en' => \Gini\Config::get('site.name_en')]);
        $this->view->footer = V('admin/footer');
    }
}

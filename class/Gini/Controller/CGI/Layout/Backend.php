<?php

namespace Gini\Controller\CGI\Layout;

class Backend extends \Gini\Controller\CGI\Layout
{
    public function __preAction($action, &$params)
    {
        parent::__preAction($action, $params);
        $this->view = V('admin/layout');
        $this->view->header = V('admin/header');
        //$this->view->aside=V('admin/aside');
        if (isset($_SESSION['username'])) {
            if ($_SESSION['username'] == 'genee') {
                $this->view->aside = V('admin/aside', ['nav' => \Gini\Config::get('site.aside')['nav_admin']]);
            } else {
                $this->view->aside = V('admin/aside', ['nav' => \Gini\Config::get('site.aside')['nav_user']]);
            }
        } else {
            $this->redirect('/login');
        }
        $this->view->footer = V('admin/footer');
    }
}

<?php

namespace Gini\Controller\CGI {

class Notices extends Layout\Base
{
    public function __index()
    {
        $notices = those('article')->orderBy('pub_date', 'd');
        $this->view->navbar->active = 'notices';
        $this->view->body = V('notices/notices', array('notices' => $notices));
        $this->view->title = '平台公告|南开大学大型仪器管理平台';
    }
}

}

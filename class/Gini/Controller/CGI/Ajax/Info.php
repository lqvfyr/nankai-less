<?php

namespace Gini\Controller\CGI\AJAX {

    class Info extends \Gini\Controller\CGI
    {
        public function actionGetInfo()
        {
            $id = $this->form()['id'];
            $rpc = \Gini\IoC::construct('\Gini\RPC', \Gini\Config::get('server.api'));
            $criteria = array();
            $criteria['ids'] = array($id);
            $return_data = $rpc->equipment->searchEquipments($criteria);
            $token = $return_data['token'];
            $equipment = $rpc->equipment->getEquipment($token);
            $view = V('equipment/info', array('equipment' => $equipment));
            $data = [
                'view' => (string) $view,
            ];

            return \Gini\IoC::construct('\Gini\CGI\Response\JSON', $data);
        }
    }

}

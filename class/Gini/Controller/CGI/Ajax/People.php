<?php

namespace Gini\Controller\CGI\AJAX {

    class People extends \Gini\Controller\CGI
    {
        public function actionCurrent()
        {
            $form = $this->form();

            $url = \Gini\Config::get('server.url');
            $view = V('current_user',
                        array(
                            'name' => $form['name'],
                            'img' => $form['img'],
                            'url' => $url,
                        ));

            return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
        }
    }
}

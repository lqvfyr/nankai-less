<?php

namespace Gini\Controller\CGI\AJAX {

class Equipments extends \Gini\Controller\CGI
{
    public function actionGetEquipments()
    {
        $rpc = \Gini\IoC::construct('\Gini\RPC', \Gini\Config::get('server.api'));
//            $form['ids']=array('1','2','3','4');
            $criteria = array();
        if (is_array($form)) {
            foreach ($form as $k => $v) {
                $criteria[$k] = T($v);
            }
        }
        $criteria['ids'] = array('29', '148', '17', '18');
            //$equipments = $rpc->equipment->search($form_data);
            $return_data = $rpc->equipment->searchEquipments($criteria);
        $token = $return_data['token'];
        $equipments = $rpc->equipment->getEquipments($token, $form['st'], 4);
        $view = V('equipments/equipments', array('equipments' => $equipments));
        $data = [
                'view' => (string) $view,
            ];

        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', $data);
    }

    public function actionGetCategories()
    {
        $rpc = \Gini\IoC::construct('\Gini\RPC', \Gini\Config::get('server.api'));
        $criteria = array();
        $categories = $rpc->equipment->getEquipmentTags($criteria);
            //$categories = $rpc->equipment->get_categories('cat');
            $view = V('equipments/categories', array('categories' => $categories));
        $data = [
                'view' => (string) $view,
            ];

        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', $data);
    }
}

}

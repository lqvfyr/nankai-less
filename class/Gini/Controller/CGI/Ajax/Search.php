<?php

namespace Gini\Controller\CGI\AJAX {

class Search extends \Gini\Controller\CGI
{
    public function actionGetEquipments()
    {
        $form = $this->form();
        $rpc = \Gini\IoC::construct('\Gini\RPC', \Gini\Config::get('server.api'));

        $form['perpage'] = $form['perpage'] ? intval($form['perpage']) : 10;
        $form['counter'] = $form['counter'] ? intval($form['counter']) : 0;
        $form['st'] = intval($form['counter']) * $form['perpage'];
        $criteria = array();
        if (isset($form['id'])) {
            $criteria['group'] = $form['id'];
        }
        foreach ($form as $k => $v) {
            $criteria[$k] = T($v);
        }
        $return_data = $rpc->equipment->searchEquipments($criteria);
        $token = $return_data['token'];
        $equipments = $rpc->equipment->getEquipments($token, $form['st'], 10);
   //         $equipments = $rpc->equipment->search($form_data);

            return \Gini\IoC::construct('\Gini\CGI\Response\JSON', (array) $equipments);
    }

    public function actionGetCategories()
    {
        $rpc = \Gini\IoC::construct('\Gini\RPC', \Gini\Config::get('server.api'));
        $criteria = array();
        $categories = $rpc->equipment->getEquipmentTags($criteria);
        $form = $this->form();

        $form_data = array();
        foreach ($form as $k => $v) {
            if ($v == '') {
                continue;
            }
            if (strpos('Content-Type', $v) || strpos('Content-Location', $v) || strpos('cookie', $v) || strpos('Content-Transfer-Encoding', $v)) {
                $v = '';
            }
            $form_data[$k] = H(T($v));
        }
        $id = $form_data['id'];
        $searchtext = $form_data['searchtext'];
        $categoryhtml = '';
        if (is_numeric($id)) {
            $category = $categories[$id]['name'];
            $categoryhtml = '<span>关键词:'.$category.'</span>';
        }
        if ($id == 'all' || $id == '') {
            $categoryhtml = '<span>全部</span>:';
        }
        if ($searchtext) {
            $categoryhtml = '<span>关键词：'.$searchtext.'</span>';
        }
        $view = V('search/categories', array('categories' => $categories));
        $data = [
                'categoryhtml' => $categoryhtml,
                'view' => (string) $view,
            ];

        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', $data);
    }
}

}

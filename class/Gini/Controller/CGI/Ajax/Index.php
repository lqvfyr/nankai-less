<?php

namespace Gini\Controller\CGI\AJAX {

class Index extends \Gini\Controller\CGI
{
    public function actionGetBackends()
    {
        $rpc = \Gini\IoC::construct('\Gini\RPC', \Gini\Config::get('server.api'));
        $backends = $rpc->auth->get_backends();
        $default_backend = $rpc->auth->get_default_backend();
        $view = V('backends', array('backends' => $backends, 'default_backend' => $default_backend));
        $data = [
                'view' => (string) $view,
            ];

        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', $data);
    }
}

}

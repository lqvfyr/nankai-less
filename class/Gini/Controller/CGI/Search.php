<?php

namespace Gini\Controller\CGI {

class Search extends Layout\Base
{
    public function __index()
    {
        $rpc = \Gini\IoC::construct('\Gini\RPC', \Gini\Config::get('server.api'));
        $form = $this->form();
        if ($form['searchtext']) {
            $searchtext = $form['searchtext'];
        }
        $form['counter'] = '0';
        $form['perpage'] = '8';
        $form['st'] = $form['counter'] * $form['perpage'];

        $form_data = array();
        foreach ($form as $k => $v) {
            $form_data[$k] = T(H($v));
        }

        $equipments = $rpc->equipment->search($form_data);

        $this->view->navbar->active = 'equipments';
        $this->view->body = V('search/index', array('searchtext' => $form_data['searchtext'], 'id' => $form_data['id']));
        $this->view->title = '仪器信息|南开大学大型仪器管理平台';
    }
}

}

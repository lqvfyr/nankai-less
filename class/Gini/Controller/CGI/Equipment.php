<?php

namespace Gini\Controller\CGI {

class Equipment extends Layout\Base
{
    public function __index()
    {
        $form = $this->form();
        $this->view->navbar->active = 'equipments';
        $this->view->body = V('equipment/index', array('id' => T(H($form['id']))));
        $this->view->title = '仪器信息|南开大学大型仪器管理平台';
    }
}

}

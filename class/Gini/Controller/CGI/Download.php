<?php

namespace Gini\Controller\CGI {

class Download extends Layout\Base
{
    public function __index()
    {
        $this->view->navbar->active = 'download';
        $this->view->body = V('download');
        $this->view->title = '文件下载|南开大学大型仪器管理平台';
    }
}

}

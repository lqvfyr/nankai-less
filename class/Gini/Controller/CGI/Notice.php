<?php

namespace Gini\Controller\CGI {

class Notice extends Layout\Base
{
    public function __index()
    {
        $id = $this->form()['id'];
        $notice = a('article', $id);
        $title = $notice->title;
        $content = $notice->content;
        $this->view->navbar->active = 'notices';
        $this->view->body = V('notices/notice', array('title' => $title, 'content' => $content));
        $this->view->title = '平台公告|南开大学大型仪器管理平台';
    }
}

}

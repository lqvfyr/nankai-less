<?php

namespace Gini\Controller\CGI;

class Admin extends Layout\Backend
{
    public function __index()
    {
        if (isset($_SESSION['username'])) {
            if ($_SESSION['username'] == 'genee') {
                $this->redirect('/user/forwardlist');
            } else {
                $this->redirect('/article/forwardlist');
            }
        } else {
            $this->redirect('/login');
        }
    }
}

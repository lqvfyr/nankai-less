<?php

namespace Gini\Controller\CLI;

class Test extends \Gini\Controller\CLI
{
    public function actionGetFromYaml()
    {
        echo var_dump(\Gini\Config::get('site.aside')['nav']);
    }
    public function actionHello()
    {
        echo "Hello,世界!\n";
    }
    public function actionImportData()
    {
        $notices_path = \Gini\Core::locateFile('data/announcements/index.json', 'nankai-less');
        $notices = json_decode(file_get_contents($notices_path), true);
        foreach ($notices as $notice) {
            $notice_path = 'data/announcements/'.$notice['content'].'.md';
            $notice_path = \Gini\Core::locateFile($notice_path, 'nankai-less');
            $content = file_get_contents($notice_path);
            $article = a('article');
            $article->title = $notice['title'];
            $article->content = $content;
            $article->pub_date = $notice['date'];
            $article->pub_person = \Gini\Config::get('admin.username');
            $article->save();
        }
    }
}

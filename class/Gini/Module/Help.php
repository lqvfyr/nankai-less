<?php

namespace Gini\Module;

class Help
{
    public static function admin_setup()
    {
        $admin = \Gini\Config::get('admin');
        $user = a('user')->whose('username')->is($admin['username']);
        if ($user->id == '') {
            $user = a('user');
            $user->username = $admin['username'];
            $user->name = $admin['name'];
            $user->password = md5($admin['password']);
            $user->email = $admin['email'];
            $user->save();
        }
    }
    public static function sendemail($user, $password)
    {
        $mail = new \Gini\Mail();
        $mail->from('support@geneegroup.com', '技术支持')
            ->to($user->email, $user->name)
            ->subject('南开大型仪器共享系统')
            ->body(null, (string) V('send', ['user' => $user, 'password' => $password]))
            ->send();
    }
    public static function create_password($pw_length = 6)
    {
        $randpwd = '';
        for ($i = 0; $i < $pw_length; ++$i) {
            $randpwd .= chr(mt_rand(97, 122));
        }

        return $randpwd;
    }
}

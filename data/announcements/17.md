<p style="text-align:center">（结算时间范围：2014年3月1日-2014年8月31日）</p>

大型仪器平台将对2014年3月1日-8月31日时间段测试收费进行结算及分配，9月10日将对该时间段产生的测试收费记录进行锁定,记录锁定后机组人员将不能对其进行修改。请机组人员务必在9月10日前对该时间段产生的送样或使用预约未扣费的记录进行扣费，以免记录锁定后无法更改而影响仪器的测试收费的统计及分配。

正值假期，请各位仪器负责人及机组人员积极配合，做好结算及使用情况统计工作。

联系电话：23503143 , 23508813

<p style="text-align:right">实验室设备处</p>
<p style="text-align:right">2014年8月25日</p>

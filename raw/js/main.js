requirejs.config({
    baseUrl: 'assets/js',
    paths: {
		'bootstrap.datetimepicker':'bootstrap-datetimepicker',
		'summernote':'summernote'

    },
    shim: {
        'bootstrap': ['jquery'],
	 	'rangy-selectionsaverestore':['rangy'],
		'summernote':{
			deps:['jquery']
		}
    }
});

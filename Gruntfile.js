/*global module:false*/
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        // Task configuration.
		gini: {
			bower: {
				options: {
				   dependencies: {
						"bootstrap": "~3.0.2",
						"font-awesome": "~4.0.3",
						"requirejs": "~2.1.9",
						"html5shiv": "~3.7.0",
						"respond": "~1.4.1",
						"require-css": "~0.1.0"
					},
				},
			},
		},
        shell: {
            bower: {
                command: [
                'bower install',
                'bower update'
                ].join('&&')
            },
            bootstrap: {
                command: [
                'npm install',
                'grunt dist'
                ].join('&&'),
                options: {
                    execOptions: {
                        cwd: 'bower_components/bootstrap'
                    }
                }
            },
        },
        uglify: {
            requirejs: {src: 'bower_components/requirejs/require.js',  dest: 'raw/assets/js/require.js'},
            requirecss: {src: 'bower_components/require-css/css.js', dest: 'raw/assets/js/css.js'}
        },
        copy: {
            bower: {
                files: [
                {src: 'bower_components/bootstrap/dist/css/bootstrap.min.css', dest: 'raw/assets/css/bootstrap.css'},
                {cwd: 'bower_components/bootstrap/dist/fonts', 'src': '*.*', dest: 'raw/assets/fonts', expand: true},
                {src: 'bower_components/bootstrap/dist/js/bootstrap.min.js', dest: 'raw/assets/js/bootstrap.js'},
                {src: 'bower_components/jquery/jquery.min.js', dest: 'raw/assets/js/jquery.js'},
                {src: 'bower_components/font-awesome/css/font-awesome.min.css', dest: 'raw/assets/css/font-awesome.css'},
                {cwd: 'bower_components/font-awesome/fonts', 'src': 'fontawesome-webfont.*', dest: 'raw/assets/fonts', expand: true},
                {src: 'bower_components/respond/dest/respond.min.js', dest: 'raw/assets/js/respond.js'},
                {src: 'bower_components/html5shiv/dist/html5shiv.js', dest: 'raw/assets/js/html5shiv.js'},
                ]
            }
        },
		watch: {
			raw: {
				files: ['raw/**/*.*', 'class/orm/**/*.php'],
				tasks: ['gini:update'],
				options: { spawn: false },
			}
		}
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-gini');

     // Default task.
    grunt.registerTask('default', ['gini:bower', 'shell', 'uglify', 'copy', 'gini:update']);
};
